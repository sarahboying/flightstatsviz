import React from 'react';

import './App.css';

import WorldMap from './WorldMap';
import Map from './Map';
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <WorldMap />
      </div>
    );
  }
}

export default App;

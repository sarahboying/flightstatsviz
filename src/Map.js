import React, { Component } from 'react';
import {
  geoMercator, geoPath, geoTransform,
} from 'd3-geo';
import {
  scaleLinear, scaleThreshold,
} from 'd3';
import { feature } from 'topojson-client';
import airportData from './airport.json';
import flightData from './one_day_sample.json';
import Flight from './Flight';

class WorldMap extends Component {
  constructor() {
    super();
    this.state = {
      worldData: [],
      airportD: null,
      airportDir: null,
      flightD: null,
      flightCount: null,
      scale: null,
    };
    this.handleCountryClick = this.handleCountryClick.bind(this);
    this.changeFlightVisibility = this.changeFlightVisibility.bind(this);
  }

  componentDidMount() {
    // fetch('https://unpkg.com/world-atlas@1/world/110m.json').then((response) => {
    //   if (response.status !== 200) {
    //     console.log(`There was a problem: ${response.status}`);
    //     return;
    //   }
    //   response.json().then((worldData) => {
    //     console.log('hello');
    //     this.setState({
    //       worldData: feature(worldData, worldData.objects.countries).features,
    //     });
    //   });
    // });

    const mapPromise = fetch('https://unpkg.com/world-atlas@1/world/110m.json');
    const airportPromise = airportData;
    const flightPromise = flightData;
    Promise.all([mapPromise, airportPromise, flightPromise]).then((dataB) => {
      console.log(dataB);
      const [map, airport, flight] = dataB;
      map.json().then((worldData) => {
        console.log('hello1');
        this.setState({
          worldData: feature(worldData, worldData.objects.countries).features,
          flightD: JSON.parse(flight),
        });
        const airportMap = new Map();
        const flightMap = new Map();
        JSON.parse(airport).map((d) => airportMap.set(d.short_name, [d.longitude, d.latitude]));
        JSON.parse(flight).map((d) => flightMap.set(d.departure_airport,
          JSON.parse(flight).reduce((n, val) => n + (val.departure_airport === d.departure_airport), 0)));
        const arcScale = scaleLinear([0, Math.max(...flightMap.values())], [1, 3]);
        const airportInFlight = JSON.parse(flightData).map((d) => d.departure_airport);
        const airportSetInFlight = new Set(airportInFlight);
        const airportDataInSample = JSON.parse(airport).filter((d) => airportSetInFlight.has(d.short_name));
        console.log(airportMap);
        this.setState({
          airportD: airportDataInSample,
          airportDir: airportMap,
          flightCount: flightMap,
          scale: arcScale,
        });
      }).then(() => {
        console.log('hello2');
      })

        .then(() => { console.log(JSON.parse(flight)[0]); });
    });
  }

  handleCountryClick(countryIndex) {
    const { worldData } = this.state;
    console.log('Clicked on a country: ', worldData[countryIndex]);
  }

  projection() {
    return geoMercator()
      .scale(150)
      .translate([1200 / 2, 800 / 2]);
  }

  matrix(a, b, c, d, tx, ty) {
    return geoTransform({
      point: (x, y) => (this.stream.point(a * x + b * y + tx, c * x + d * y + ty)),
    });
  }

  changeFlightVisibility(flightCountry) {
    const { flightD } = this.state;
    const filteredFlight = flightD.filter((d) => d.departure_airport === flightCountry);
    const flightMap = new Map();

    filteredFlight.map((d) => flightMap.set(d.departure_airport,
      filteredFlight.reduce((n, val) => n + (val.departure_airport === d.departure_airport), 0)));
    this.setState({
      flightCount: flightMap,
    });
  }

  render() {
    const {
      worldData, airportDir, flightD, airportD, scale,
    } = this.state;

    const countries = (!worldData) ? null : (worldData.map((d, i) => (
      <path
        transform="matrix(1 0 0 1 0 0)"
        key={`path-${i}`}
        d={geoPath().projection(this.projection())(d)}
        className="country"
        fill={`rgba(38,50,56,${(1 / worldData.length) * i})`}
        stroke="#ffffff"
        strokeWidth={0.5}
        onClick={() => this.handleCountryClick(i)}
      />
    )));
    console.log(airportD);
    console.log(flightD);
    console.log(this.matrix(1, 0, 0, 1, 0, 0));
    // console.log(flightCount);
    // const latitude = (!airportDir.filter((d) => d.short_name === 'GKA')[0]) ? null : (airportDir.filter((d) => d.short_name === 'GKA')[0].index);
    // const { longitude } = airportData.filter((d) => +d.short_name === +f.departure_airport);
    // console.log(latitude);
    // console.log(longitude);
    const airportCircle = (!airportD) ? null : (airportD.map((d, i) => (
      <circle
        transform="matrix(1 0 0 1 0 0)"
        key={`dot-${i}`}
        cx={this.projection()([d.longitude, d.latitude])[0]}
        cy={this.projection()([d.longitude, d.latitude])[1]}
        r={2}
        fill="#E91E63"
        className="marker"
      />
    )));

    const SFOCircle = (!airportDir) ? null : (
      <circle
        transform="matrix(1 0 0 1 0 0)"
        key="sfo-dot"
        cx={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[0]}
        cy={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[1]}
        r={3}
        fill="#00A0A0"
        className="marker"
      />
    );
    const { flightCount } = this.state;
    const flightArc = (!airportDir || !flightCount) ? null : (Array.from(flightCount.keys()).map((key) => (
      <line
        transform="matrix(1 0 0 1 0 0)"
        key={`flight-line-${key}`}
        x1={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[0]}
        y1={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[1]}
        x2={this.projection()([airportDir.get(key)[0], airportDir.get(key)[1]])[0]}
        y2={this.projection()([airportDir.get(key)[0], airportDir.get(key)[1]])[1]}
        className="flight-line"
        // fill={`rgba(38,50,56,${(1 / worldData.length) * i})`}
        stroke="#6e88fa"
        strokeWidth={scale(flightCount.get(key))}
        display="inline"
        // onClick={this.changeFlightVisibility(key)}
      />
    )));

    const flightArcC = (!airportDir || !flightCount) ? null : (
      <Flight
        airportDir={airportDir}
        flightCount={flightCount}
        scale={scale}
        onMouseOver={this.changeFlightVisibility}
      />
    );
    return (
      <svg width={1200} height={800} viewBox="0 0 1200 800">
        <g className="countries" transform="matrix(1 0 0 1 0 0)">
          {countries}
        </g>
        <g className="markers">
          {airportCircle}
          {SFOCircle}

        </g>
        <g className="lines">
          {flightArc}

        </g>
      </svg>
    );
  }
}

export default WorldMap;

import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { loadModules, loadCss } from 'esri-loader';
import { scaleLinear } from 'd3';
import * as d3 from 'd3-collection';
import './WorldMap.css';
import airportData from './airport.json';
import flightData from './one_day_sample';

const styles = {
  container: {
    height: '84vh',
    width: '100vw',
  },
  mapDiv: {
    padding: 0,
    margin: 0,
    height: '100%',
    width: '100%',
  },

  info: {
    backgroundColor: 'black',
    opacity: 0.75,
    color: 'orange',
    fontSize: '18pt',
    padding: '8px',
    visibility: 'visible',
  },
};

class WorldMap extends React.Component {
  constructor(props) {
    super(props);
    this.info = React.createRef();
    loadCss();
    this.state = {
      id: null,
      // vm: null,
      // maxZoomed: false,
      // minZoomed: false,
      // view: null,
    };
    // this.onViewLoaded = this.onViewLoaded.bind(this);
    // this.onZoomChange = this.onZoomChange.bind(this);
    // this.zoomIn = this.zoomIn.bind(this);
    // this.zoomOut = this.zoomOut.bind(this);
  }

  componentDidMount() {
    loadModules([
      'esri/Map',
      'esri/views/MapView',
      'esri/core/watchUtils',
      'esri/widgets/Zoom/ZoomViewModel',
      'esri/Graphic', 'esri/layers/GraphicsLayer',
      'esri/core/promiseUtils',
      'esri/layers/FeatureLayer',
    ]).then(([Map, MapView, watchUtils, ZoomViewModel, Graphic, GraphicsLayer, promiseUtils, FeatureLayer]) => {
      this.map = new Map({ basemap: 'topo' });
      const view = MapView({
        container: 'viewDiv',
        map: this.map,
        zoom: 4,
        center: [-122.381147, 37.617591],
        highlightOptions: {
          color: 'orange',
        },
        ui: {
          components: ['attribution'], // empty the UI, except for attribution
        },
      });
      // view.ui.add(document.createElement('div'), 'bottom-left');
      this.setState({ watchUtils, vm: new ZoomViewModel(), view });
      view.when(this.onViewLoaded);

      const airportPromise = airportData;
      const flightPromise = flightData;
      // const flightPromise2 = new Promise(((resolve) => {
      //   setTimeout(() => {
      //     resolve(connect("SELECT * from air_datamart.vw_arrivals_flight_info where actual_arrival between '2019-11-25' and '2019-11-26' ;"));
      //   }, 300);
      // }));
      Promise.all([airportPromise, flightPromise]).then((data) => {
        const [airport, flight] = data;
        const airportMap = new Map();
        const flightMap = new Map();
        const flightCount = d3.nest()
          .key((d) => d.departure_airport)
          .rollup((v) => v.length)
          .entries(flight);
        const airportJson = JSON.parse(airport);
        console.log(flightCount);
        const flightJson = JSON.parse(flight);
        airportJson.map((d) => airportMap.set(d.short_name, [d.longitude, d.latitude]));

        flightJson.map((d) => flightMap.set(d.departure_airport,
          flightJson.reduce((n, val) => n + (val.departure_airport === d.departure_airport), 0)));
        // console.log(Array.from(airportMap.values()));
        this.setState(flightMap);
        const arcScale = scaleLinear([0, Math.max(Object.keys(flightMap).map((i) => flightMap[i]).slice(2))], [1, 10]);
        const airportInFlight = flightJson.map((d) => d.departure_airport);
        const airportSetInFlight = new Set(airportInFlight);
        const airportDataInSample = airportJson.filter((d) => airportSetInFlight.has(d.short_name));
        const airportPointGraphic = [];
        // console.log((Object.keys(flightMap).map((i) => flightMap[i]).slice(2)));
        console.log(airportDataInSample);

        airportPointGraphic.push(new Graphic({
          geometry: {
            type: 'point',
            longitude: -122.3790,
            latitude: 37.6213,
          },
          symbol: {
            type: 'simple-marker',
            size: 5,
            color: [0, 128, 128], // teal
            outline: {
              color: [255, 255, 255], // white
              width: 0.5,
            },
          },
        }));
        airportDataInSample.map((d) => airportPointGraphic.push(new Graphic({
          geometry: {
            type: 'point',
            longitude: d.longitude,
            latitude: d.latitude,
          },
          symbol: {
            type: 'simple-marker',
            size: 5,
            color: [226, 119, 40], // orange
            outline: {
              color: [255, 255, 255], // white
              width: 0.5,
            },
          },
          popupTemplate: {
            title: `${d.short_name}`,
            content:
                `<p>${d.airport_name}</p><p><elevation:${d.elevation}</p><p>Latitude: ${d.latitude}</p><p>Longitude: ${d.longitude}</p>`
            + `<p>Elevation: ${d.elevation}</p></p><p>Timezone: ${d.time_zone}</p><p>City: ${d.city}</p>`,
          },
        })));
        const flightLineGraphic = airportDataInSample.map((d) => (new Graphic({
          attributes: {
            flightCount: flightMap.get(d.short_name),
            destination: d.short_name,
          },
          geometry: {
            type: 'polyline',
            paths: [[-122.3790, 37.6213],
              [airportMap.get(d.short_name)[0], airportMap.get(d.short_name)[1]]],
          },
          symbol: {
            type: 'simple-line',
            width: flightMap.get(d.short_name) / 100 * 10,
            color: [0, 128, 128], // teal
          },
          popupTemplate: {
            title: `SFO -> ${d.short_name}`,
            content:
              `<p>Flight Count: ${flightMap.get(d.short_name)}</p>`,
          },
        })
        ));


        const airportLayer = new GraphicsLayer({ graphics: airportPointGraphic });
        // const flightLayer = new GraphicsLayer({ graphics: flightLineGraphic });
        // flightLayer.addMany(airportPointGraphic+ flightLineGraphic);
        view.map.add(airportLayer);
        view.map.add('info', 'top-bottom');
        // view.map.add([airportLayer, flightLayer]);
        // view.map.add(flightLayer);
        // view.ui.add('info', 'top-right');
        view.popup.on('trigger-action', (event) => {
          // If the zoom-out action is clicked, fire the zoomOut() function
          if (event.action.id === 'see-airport') {
            this.zoomOut();
          }
        });
        return flightLineGraphic;
      }).then((graphics) => {
        console.log(graphics);
        const flightLineFeatureLayer = new FeatureLayer({
          source: graphics,
          objectIdField: 'OBJECTID',
          fields: [
            {
              name: 'OBJECTID',
              type: 'oid',
            },

            {
              name: 'destination',
              type: 'string',
            },


            {
              name: 'FlightCount',
              type: 'integer',
            },

          ],
          geometryType: 'polyline',

          renderer: {
            type: 'simple', // autocasts as new SimpleRenderer()
            symbol: {
              type: 'simple-line', // autocasts as new SimpleLineSymbol()
              color: [0, 128, 128],
              width: 1,
            // outline: { // autocasts as new SimpleLineSymbol()
            //   width: 0.5,
            //   color: 'white',
            // },
            },

          },
        });
        this.flightLineFeatureLayer = flightLineFeatureLayer;
        return flightLineFeatureLayer;
      })
        .then((layer) => {
          view.map.add(layer);
          return layer.when();
        })
        .then((layer) => {
          const renderer = layer.renderer.clone();

          layer.renderer = renderer;
          return view.whenLayerView(layer);
        })
        .then((layerView) => {
          let highlight = null; let currentId;
          const eventhandler = (event) => {
            view.hitTest(event).then(
              (response) => {
                const { flightLineFeatureLayer } = this;
                // console.log(flightLineFeatureLayer);
                if (response.results.length && flightLineFeatureLayer) {
                  // console.log(response.results.length);
                  const matchedGraphic = response.results.filter((result) => result.graphic.layer === flightLineFeatureLayer)[0].graphic;
                  // const { attributes } = matchedGraphic;
                  console.log(matchedGraphic);
                  const id = matchedGraphic.attributes.OBJECTID;
                  const destination = matchedGraphic.attributes.destination;
                  const count = matchedGraphic.attributes.FlightCount;

                  if (highlight !== null && (currentId !== id) && (currentId !== null)) {
                    console.log(highlight);
                    console.log(currentId);
                    console.log(id);
                    highlight.remove();
                    highlight = null;
                    return;
                  }

                  if (highlight) {
                    return;
                  }

                  this.info.current.innerHTML = `${count} flights from SFO to ${destination} on Nov, 25, 2019`;

                  const query = layerView.createQuery();
                  query.where = `OBJECTID = ${id}`;

                  layerView.queryObjectIds(query).then((ids) => {
                    // console.log(query);
                    console.log(highlight);
                    if (highlight !== null) {
                      console.log(highlight);
                      highlight.remove();
                      highlight = null;
                    }
                    highlight = layerView.highlight(ids);
                    console.log(highlight);
                    currentId = id;
                  });
                } else {
                  // remove the highlight if no features are
                  // returned from the hitTest
                  highlight.remove();
                  highlight = null;
                  // document.getElementById("info").style.visibility = "hidden";
                }
              },
            );
          };
          view.on('pointer-move', eventhandler);
          view.on('pointer-down', eventhandler);
        });
    });
  }

  getGraphics(response) {
    if (response.results.length) {
      console.log(response.results);
    }
  }
  // onViewLoaded(view) {
  //   const { watchUtils, vm } = this.state;
  //   vm.view = view;
  //   watchUtils.init(view, 'zoom', this.onZoomChange);
  // }

  // onZoomChange(value) {
  //   const { view } = this.state;
  //   this.setState({
  //     maxZoomed: value === view.constraints.maxZoom,
  //     minZoomed: value === view.constraints.minZoom,
  //   });
  // }

  // zoomIn() {
  //   const { maxZoomed, vm } = this.state;
  //   if (!maxZoomed) {
  //     vm.zoomIn();
  //   }
  // }
  //
  // zoomOut() {
  //   const { minZoomed, vm } = this.state;
  //   if (!minZoomed) {
  //     vm.zoomOut();
  //   }
  // }

  render() {
    // const { maxZoomed, minZoomed } = this.state;
    // const maxstate = maxZoomed
    //   ? 'button circle raised disable'
    //   : 'button circle raised';
    // const minstate = minZoomed
    //   ? 'button circle raised disable'
    //   : 'button circle raised';
    const { info } = this.state;

    return (
      <div className="WorldMap">
        <div style={styles.container}>
          <div id="viewDiv" style={styles.mapDiv}>
            {/* <div className={maxstate} onClick={this.zoomIn}> */}
            {/*  <Icon image="zoom-in">+</Icon> */}
            {/* </div> */}
            {/* <div className={minstate} onClick={this.zoomOut}> */}
            {/*  <Icon image="zoom-out">-</Icon> */}
            {/* </div> */}
          </div>
          <div id="info">
            <span id="id" ref={this.info}>{info}</span>
            {' '}
            <br />
             {/*<span id="departure" ref='departure'></span> <br /> */}
             {/*<span id="arrival" ref='arrival'></span> */}
          </div>
        </div>
      </div>

    );
  }
}

export default WorldMap;

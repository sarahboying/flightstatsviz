import React from 'react';



export default class Flight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      airportDir: this.props.airport,
      flightCount: this.props.flight,
      scale: this.props.scale,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.flightCount !== this.props.flight) {
      this.setState({
        flightCount: this.props.flight,

      });
    }
  }

  render() {
    const { flightCount, airportDir, scale } = this.state;
    const { onMouseOver } = this.props;
    const flightArc = (!airportDir || !flightCount) ? null : (Array.from(flightCount.keys()).map((key) => (
      <line
        key={`flight-line-${key}`}
        x1={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[0]}
        y1={this.projection()([airportDir.get('SFO')[0], airportDir.get('SFO')[1]])[1]}
        x2={this.projection()([airportDir.get(key)[0], airportDir.get(key)[1]])[0]}
        y2={this.projection()([airportDir.get(key)[0], airportDir.get(key)[1]])[1]}
        className="flight-line"
                // fill={`rgba(38,50,56,${(1 / worldData.length) * i})`}
        stroke="#6e88fa"
        strokeWidth={scale(flightCount.get(key))}
        display="inline"
        onMouseOver={onMouseOver(key)}
      />
    )));
    return (
      <g>

        {flightArc}


      </g>

    );
  }
}
